<?php

	/**
	* Plugin Name: UVjagtPro - Custom favicon
	* Description: This plugin adds a custom facivon to theme.
	* Author: Kim Nyegaard Andreasen
	* Version: 1.0
	*/

	function childtheme_favicon() 
    { ?>
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri() ?>/img/favicon.ico">
    <?php }
     
    add_action('wp_head', 'childtheme_favicon');

?>