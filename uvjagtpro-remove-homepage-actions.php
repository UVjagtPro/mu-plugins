<?php

	/**
	* Plugin Name: UVjagtPro - Removing home page actions
	* Description: This plugin removes the home page action provided by WooCommerce plugin.
	* Author: Kim Nyegaard Andreasen
	* Version: 1.0
	*/

	/**
	Reference: 
	- https://wpthememakeover.com/2016/02/23/how-to-change-the-storefront-theme-homepage-no-plugins/
	**/

	function storefront_child_remove_home_page_content(){
		remove_action('homepage', 'storefront_homepage_content', 10);
		remove_action('homepage', 'storefront_product_categories', 20);
		remove_action('homepage', 'storefront_recent_products', 30);
		remove_action('homepage', 'storefront_featured_products', 40);
		remove_action('homepage', 'storefront_popular_products', 50);
		remove_action('homepage', 'storefront_on_sale_products', 60);
		remove_action('homepage', 'storefront_best_selling_products', 70);
	}

	add_action('init','storefront_child_remove_home_page_content');

?>
