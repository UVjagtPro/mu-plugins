<?php

	/**
	* Plugin Name: UVjagtPro - Hide WordPress version
	* Description: This plugin hides the WordPress version from scripts and styles.
	* Author: Kim Nyegaard Andreasen
	* Version: 1.0
	*/

	function fjarrett_remove_wp_version_strings( $src ) 
	{
	     global $wp_version;
	     parse_str(parse_url($src, PHP_URL_QUERY), $query);
	     if ( !empty($query['ver']) && $query['ver'] === $wp_version ) 
	     {
	          $src = remove_query_arg('ver', $src);
	     }

	     return $src;
	}

	add_filter( 'script_loader_src', 'fjarrett_remove_wp_version_strings' );

	add_filter( 'style_loader_src', 'fjarrett_remove_wp_version_strings' );

	/* Hide WP version strings from generator meta tag */

	function wpmudev_remove_version() 
	{
	    return '';
	}

	add_filter('the_generator', 'wpmudev_remove_version');

?>